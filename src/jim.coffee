# This file is a part of CoffeeChat

events = require "events"
jsonsp = require "jsonsp"

exports.wrapSocket = (socket) ->
  emitter = new events.EventEmitter()
  parser = new jsonsp.Parser()
  socket.on "data", (data) ->
    parser.feed data.toString 'utf8'
  parser.on 'object', (obj) ->
    emitter.emit 'data', obj
  emitter.on 'write', (obj) ->
    socket.write JSON.stringify obj
  emitter.write = (obj) ->
    @emit 'write', obj
  emitter
