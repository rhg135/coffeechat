# This file is a part of coffeechat

utils = require "./utils"
jim = require "./jim"
net = require "net"

exports.connect = (host, port, onError) ->
  # build a socket
  options = {
    host: host,
    port: port
  }
  sock = net.connect options

  sock.on 'error', onError
  jim.wrapSocket sock

exports.write = (jim, obj) ->
  console.log "Sending #{JSON.stringify(obj)}"
  jim.write obj
