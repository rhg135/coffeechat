# This file is a part of coffeechat

exports.onError = (err) ->
  console.log "I should care, but I don't"
  console.log err

exports.makeSync = (f, wait, handle) ->
  val = undefined
  callback = (err, data) ->
    handler err if err
    val = data
  f callback
  wait() until val
  val
