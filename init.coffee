# This file is a part of coffeechat

connect = require("./src/coffeechat").connect
utils = require './src/utils'

HOST = "localhost"
PORT = 4004

start = (host, port) ->
  try
    jim = connect host, port, utils.onError
    jim.on "data", (obj) ->
      console.log "<-- "
      console.log obj
    console.log connect
  catch er
    utils.onError er

start HOST, PORT
